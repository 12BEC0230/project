package parking;

import com.shivendra.parking.caching.CurrentParkingStat;
import com.shivendra.parking.domain.Parking;

/**
 * Created by spsingh on 7/13/19.
 */
public class ParkingUtils {


    public static Parking createParking(int numberOfFloors, int numberOfRows, int numberOfBikeSpotPerRow, int
            numberOfCarSpotPerRow, int numberOfBusSpotPerRow) {
        Parking.init(numberOfFloors);
        Parking parking = Parking.getInstance();
        parking.construct(numberOfFloors, numberOfRows, numberOfBikeSpotPerRow, numberOfCarSpotPerRow, numberOfBusSpotPerRow);
        return parking;
    }

    public static CurrentParkingStat createStats(int availableCarSpot, int availableBikeSpot, int availableBusSpot) {
        CurrentParkingStat.init(availableCarSpot, availableBikeSpot, availableBusSpot);
        CurrentParkingStat currentParkingStat = CurrentParkingStat.getInstance();
        return currentParkingStat;
    }

}
