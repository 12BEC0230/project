package parking;

import com.shivendra.parking.caching.CurrentParkingStat;
import com.shivendra.parking.domain.Parking;
import com.shivendra.parking.domain.VehicleType;
import com.shivendra.parking.publisher.ParkingStatsPublisher;
import com.shivendra.parking.service.ParkingServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * The (@code ParkingSlotsExpiredTests) tests various cases of vehicles exit
 */
public class ParkingSlotsExpiredTests {

    Parking parking;
    CurrentParkingStat parkingStat;
    ParkingStatsPublisher parkingStatsPublisher = Mockito.mock(ParkingStatsPublisher.class);
    ParkingServiceImpl parkingService = new ParkingServiceImpl();

    /*
    * Here the parking system is set to number of floors as 1 and slots for car and
    * bike in each row is 1 and bus is 5.
    * */
    @Before
    public void setup() {
        parking = ParkingUtils.createParking(1, 1, 1, 1, 5);
        CurrentParkingStat.init(1, 1, 5);
        parkingStat = ParkingUtils.createStats(1, 1, 5);
        parkingService.setParkingStatsPublisher(parkingStatsPublisher);
    }

    /*
    * Here the bus with bus numbers 1 and 2 is parked spots available is 3 and when bus with bus number 1
    * exits number of available bus spots is 4
    */
    @Test
    public void testBusExit() {
        parkingService.acquireSpot(VehicleType.BUS, parking, "1", parkingStat);
        assert parkingStat.getAvailableBusSpot() == 4;
        parkingService.acquireSpot(VehicleType.BUS, parking, "2", parkingStat);
        assert parkingStat.getAvailableBusSpot() == 3;
        parkingService.expireSpot(VehicleType.BUS, parking, "1", parkingStat);
        assert parkingStat.getAvailableBusSpot() == 4;
    }

    /*
    Here the car with car number 1 is parked.. spots available is 0 and when car with car number 1
    exits number of available bus spots is 1
    */
    @Test
    public void testCarExit() {
        parkingService.acquireSpot(VehicleType.CAR, parking, "1", parkingStat);
        assert parkingStat.getAvailableCarSpot() == 0;
        parkingService.expireSpot(VehicleType.CAR, parking, "1", parkingStat);
        assert parkingStat.getAvailableCarSpot() == 1;
    }

    /*
  Here the bike with bike number 1 is parked.. spots available is 0 and when bike with bike number 1
  exits number of available bike spots is 1
  */
    @Test
    public void testBikeExit() {
        parkingService.acquireSpot(VehicleType.BIKE, parking, "1", parkingStat);
        assert parkingStat.getAvailableBikeSpot() == 0;
        parkingService.expireSpot(VehicleType.BIKE, parking, "1", parkingStat);
        assert parkingStat.getAvailableBikeSpot() == 1;
    }

    /*
    Here the bike with bike number 1 is parked.. spots available is 0 and when bike with bike number 2
    is asked to exit number of available bike spots is 0 since bike with number does not exist
    */
    @Test
    public void testBikeExitWithWrongBikeNumber() {
        parkingService.acquireSpot(VehicleType.BIKE, parking, "1", parkingStat);
        assert parkingStat.getAvailableBikeSpot() == 0;
        parkingService.expireSpot(VehicleType.BIKE, parking, "2", parkingStat);
        assert parkingStat.getAvailableBikeSpot() == 0;
    }
    /*
    Here the car with car number 1 is parked.. spots available is 0 and when car with car number 2
    is asked to exit number of available car spots is 0 since car with number does not exist
    */
    @Test
    public void testCarExitWithWrongCarNumber() {
        parkingService.acquireSpot(VehicleType.CAR, parking, "1", parkingStat);
        assert parkingStat.getAvailableCarSpot() == 0;
        parkingService.expireSpot(VehicleType.CAR, parking, "2", parkingStat);
        assert parkingStat.getAvailableCarSpot() == 0;
    }
}
