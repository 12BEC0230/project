package parking;


import com.shivendra.parking.caching.CurrentParkingStat;
import com.shivendra.parking.domain.Parking;
import com.shivendra.parking.domain.VehicleType;
import com.shivendra.parking.publisher.ParkingStatsPublisher;
import com.shivendra.parking.service.ParkingServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.doNothing;


/**
 * The (@code ParkingSlotsAquiredTests) tests various cases of vehicles entry
 * */

public class ParkingSlotsAquiredTests {


    Parking parking;
    CurrentParkingStat parkingStat;
    ParkingStatsPublisher parkingStatsPublisher = Mockito.mock(ParkingStatsPublisher.class);
    ParkingServiceImpl parkingService = new ParkingServiceImpl();

    @Before
    public void setup() {
        parking = ParkingUtils.createParking(1, 1, 1, 1, 5);
        CurrentParkingStat.init(1, 1, 5);
        parkingStat = ParkingUtils.createStats(1, 1, 5);
        parkingService.setParkingStatsPublisher(parkingStatsPublisher);
    }

    /*Here the bus with bus number 1 is parked.Available spots after bus is parked is 4
    * */
    @Test
    public void acquireBusSpotTest() {
        doNothing().when(parkingStatsPublisher).publish(parkingStat);
        parkingService.acquireSpot(VehicleType.BUS, parking, "1", parkingStat);
        assert parkingStat.getAvailableBusSpot() == 4;
    }

    /*
    * Here the Bike withn bike number 1 is parked.Available spots for bike after the bike is parked is now 0*/
    @Test
    public void acquireBikeSpotTest() {
        doNothing().when(parkingStatsPublisher).publish(parkingStat);
        parkingService.acquireSpot(VehicleType.BIKE, parking, "1", parkingStat);
        assert parkingStat.getAvailableBikeSpot() == 0;
    }

    /*
    * Here the car with car number 1 is parked . Availabe Spots for car after the car is parked is now 0
    * */
    @Test
    public void acquireCarSpotTest() {
        doNothing().when(parkingStatsPublisher).publish(parkingStat);
        parkingService.acquireSpot(VehicleType.CAR, parking, "1", parkingStat);
        assert parkingStat.getAvailableCarSpot() == 0;
    }

    /*
    * Here the car with car number 1 is parked . Availabe Spots for car after the car is parked is now 0.
    * When another car with number comes to park parking space is not available
    * */
    @Test
    public void acquireCarSpotWhenParkingFullTest() {
        doNothing().when(parkingStatsPublisher).publish(parkingStat);
        parkingService.acquireSpot(VehicleType.CAR, parking, "1", parkingStat);
        assert parkingStat.getAvailableCarSpot() == 0;
        parkingService.acquireSpot(VehicleType.CAR, parking, "2", parkingStat);
        assert parkingStat.getAvailableCarSpot() == 0;
    }

}
