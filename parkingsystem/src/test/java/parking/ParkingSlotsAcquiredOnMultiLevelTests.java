package parking;

import com.shivendra.parking.caching.CurrentParkingStat;
import com.shivendra.parking.domain.Parking;
import com.shivendra.parking.domain.VehicleType;
import com.shivendra.parking.publisher.ParkingStatsPublisher;
import com.shivendra.parking.service.ParkingServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.doNothing;

/**
 * Created by spsingh on 7/13/19.
 */
public class ParkingSlotsAcquiredOnMultiLevelTests {
    Parking parking;
    CurrentParkingStat parkingStat;
    ParkingStatsPublisher parkingStatsPublisher = Mockito.mock(ParkingStatsPublisher.class);
    ParkingServiceImpl parkingService = new ParkingServiceImpl();

    @Before
    public void setup() {
        parking = ParkingUtils.createParking(2, 2, 2, 2, 5);
        CurrentParkingStat.init(2*2*2, 2*2*2, 5*2*2);
        parkingStat = ParkingUtils.createStats(2*2*2, 2*2*2, 5*2*2);
        parkingService.setParkingStatsPublisher(parkingStatsPublisher);
    }
    /*Here the bus with bus number 1 is parked.Available spots after bus is parked is 19
   * */
    @Test
    public void acquireBusSpotTest() {
        doNothing().when(parkingStatsPublisher).publish(parkingStat);
        assert parkingStat.getAvailableBusSpot() == 20;
        parkingService.acquireSpot(VehicleType.BUS, parking, "1", parkingStat);
        assert parkingStat.getAvailableBusSpot() == 19;
    }

    /*
   * Here the Bike withn bike number 1 is parked.Available spots for bike after the bike is parked is now 7 */
    @Test
    public void acquireBikeSpotTest() {
        doNothing().when(parkingStatsPublisher).publish(parkingStat);
        assert parkingStat.getAvailableBikeSpot() == 8;
        parkingService.acquireSpot(VehicleType.BIKE, parking, "1", parkingStat);
        assert parkingStat.getAvailableBikeSpot() == 7;
    }

    @Test
    public void acquireCarSpotTest() {
        doNothing().when(parkingStatsPublisher).publish(parkingStat);
        assert parkingStat.getAvailableCarSpot() == 8;
        parkingService.acquireSpot(VehicleType.CAR, parking, "1", parkingStat);
        assert parkingStat.getAvailableCarSpot() == 7;
    }


}
