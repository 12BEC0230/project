package com.shivendra.parking.domain;

import java.io.Serializable;

/**
 * Created by spsingh on 7/11/19.
 */
public class ParkingRow implements Serializable {
    public String[] carSlots;
    public String[] bikeSlots;
    public String[] busSlots;


    public ParkingRow(int carSpot, int bikeSpot, int busSpot) {
        this.carSlots = new String[carSpot];
        this.bikeSlots = new String[bikeSpot];
        this.busSlots = new String[busSpot];
    }


    public String[] getCarSlots() {
        return carSlots;
    }

    public String[] getBikeSlots() {
        return bikeSlots;
    }

    public String[] getBusSlots() {
        return busSlots;
    }
}
