package com.shivendra.parking.domain;

/**
 * Created by spsingh on 7/11/19.
 */
public class ParkingFloor {

    ParkingRow[] parkingRows;

    public ParkingFloor(int numberOfRows) {
        parkingRows = new ParkingRow[numberOfRows];
    }

    public ParkingRow[] getParkingRows() {
        return parkingRows;
    }

    public void setParkingRows(ParkingRow[] parkingRows) {
        this.parkingRows = parkingRows;
    }
}
