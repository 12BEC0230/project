package com.shivendra.parking.domain;

/**
 * Created by spsingh on 7/11/19.
 */
public class Parking {


    private  static Parking parking;
    private ParkingFloor[] parkingFloors;

    public Parking(int numberOfFloors) {
        parkingFloors = new ParkingFloor[numberOfFloors];
    }

    //Singleton Instance
    public static Parking getInstance() {
        if (parking == null) {
            System.out.println("initialize first ");
        }

        return parking;
    }

    public synchronized static Parking init(int numberOfFloors) {
        if (parking == null) {
            parking = new Parking(numberOfFloors);
        }
        return parking;
    }


    public ParkingFloor[] getParkingFloors() {
        return parkingFloors;
    }

    public ParkingFloor[] construct(int numberOfFloors, int numberOfRows, int smallSpotPerRow, int mediumSpotPerRow, int
            largeSpotPerRow) {
        for (int j = 0; j < numberOfFloors; j++) {
            ParkingFloor parkingFloor1 = new ParkingFloor(numberOfRows);
            ParkingRow[] parkingRows = new ParkingRow[numberOfRows];
            for (int i = 0; i < numberOfRows; i++) {
                ParkingRow parkingRow = new ParkingRow(smallSpotPerRow, mediumSpotPerRow, largeSpotPerRow);
                parkingRows[i] = parkingRow;
            }
            parkingFloor1.setParkingRows(parkingRows);
            parkingFloors[j] = parkingFloor1;
        }

        return parkingFloors;
    }
}
