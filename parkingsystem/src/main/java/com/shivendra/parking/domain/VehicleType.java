package com.shivendra.parking.domain;

/**
 * Created by spsingh on 7/11/19.
 */
public enum VehicleType {
    BUS,
    CAR,
    BIKE;
}
