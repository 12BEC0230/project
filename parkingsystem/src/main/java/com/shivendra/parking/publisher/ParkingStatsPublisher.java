package com.shivendra.parking.publisher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shivendra.parking.Util.CallableClass;
import com.shivendra.parking.Util.ParkingUtil;
import com.shivendra.parking.caching.CurrentParkingStat;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 * The (@code ParkingStatsPublisher) class publishes the updated parking stats on kafka topic..
 * the updated Stats  can be consumed in other Microservices for various business logics e.g.
 * displaying the updated slots , surging prices if number of slots are less etc.
 */
public class ParkingStatsPublisher {
    public void publish(CurrentParkingStat currentParkingStat) {
        ObjectMapper objectMapper = new ObjectMapper();
        Producer<Long, String> producer=null;
        try {
            producer = ParkingUtil.createProducer();
            String s = objectMapper.writeValueAsString(currentParkingStat);
            CallableClass callableClass = new CallableClass();
            ProducerRecord<Long,String> producerRecord = new ProducerRecord<Long, String>
                    ("stats",s);
            producer.send(producerRecord,callableClass);
            System.out.println("Latest Parking Stats published on stats topic");
        } catch (Exception e) {
            System.out.println("Problem in publish to kafka server . " + e.getMessage());
        }
    }
}
