package com.shivendra.parking.Util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shivendra.parking.caching.CurrentParkingStat;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

/**
 * Created by spsingh on 7/12/19.
 */
public class ParkingStatsSerializer implements Serializer {
    public void configure(Map configs, boolean isKey) {

    }

    public byte[] serialize(String s, Object o) {
        byte[] retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            retVal = objectMapper.writeValueAsString((CurrentParkingStat) o).getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retVal;
    }

    public byte[] serialize(String topic, Headers headers, Object data) {
        return new byte[0];
    }

    public void close() {

    }
}
