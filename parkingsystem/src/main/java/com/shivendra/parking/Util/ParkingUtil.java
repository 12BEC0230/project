package com.shivendra.parking.Util;

import com.shivendra.parking.domain.ParkingRow;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;

import java.util.Collections;
import java.util.Properties;

/**
 * Created by spsingh on 7/11/19.
 */
public class ParkingUtil {
    public static String KAFKA_BROKERS = "localhost:9092";
    public static String TOPIC_NAME = "stats";
    public static String GROUP_ID_CONFIG = "consumerGroup1";
    public static String OFFSET_RESET_EARLIER = "earliest";
    public static Integer MAX_POLL_RECORDS = 1;

    public static Producer<Long, String> createProducer() {
        Properties props = new Properties();
        props.put("bootstrap.servers",KAFKA_BROKERS);
        props.put("key.serializer", LongSerializer.class.getName());
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return new KafkaProducer(props);
    }

    public static Consumer<Long, ParkingRow> createConsumer() {
        Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_BROKERS);
        props.put("group.id", GROUP_ID_CONFIG);
        props.put("key.deserializer", LongDeserializer.class.getName());
        props.put("value.deserializer", "com.shivendra.parking.Util.ParkingStatsDeserializer");
        props.put("max.poll.records", MAX_POLL_RECORDS);
        props.put("enable.auto.commit", "false");
        props.put("auto.offset.reset", OFFSET_RESET_EARLIER);
        Consumer<Long, ParkingRow> consumer = new KafkaConsumer(props);
        consumer.subscribe(Collections.singletonList(TOPIC_NAME));
        return consumer;

    }

}
