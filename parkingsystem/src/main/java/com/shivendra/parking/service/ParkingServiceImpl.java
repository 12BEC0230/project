package com.shivendra.parking.service;

import com.shivendra.parking.caching.CurrentParkingStat;
import com.shivendra.parking.domain.Parking;
import com.shivendra.parking.domain.ParkingFloor;
import com.shivendra.parking.domain.ParkingRow;
import com.shivendra.parking.domain.VehicleType;
import com.shivendra.parking.publisher.ParkingStatsPublisher;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The (@code ParkingServiceImpl) is an implementation of ParkingService.Implements acquireSpot when
 * a vehicle is parked on available spot in any of the rows of levels in parking system.
 * Implements expireSpots when any vehicle leaves the spot.In both cases the stats cache is updated and
 * lastest stats are published
 */


public class ParkingServiceImpl implements ParkingService {


    ParkingStatsPublisher parkingStatsPublisher = new ParkingStatsPublisher();
    Lock lockOnBusAquire = new ReentrantLock();
    Lock lockOnBikeAquire = new ReentrantLock();
    Lock lockOnCarAquire = new ReentrantLock();

    Lock lockOnBusExpire = new ReentrantLock();
    Lock lockOnBikeExpire = new ReentrantLock();
    Lock lockOnCarExpire = new ReentrantLock();


    public void getParkingStat(CurrentParkingStat currentParkingStat) {
        System.out.println("Number of bus spots available : " + currentParkingStat.getAvailableBusSpot());
        System.out.println("Number of car spots available : " + currentParkingStat.getAvailableCarSpot());
        System.out.println("Number of bike spots available : " + currentParkingStat.getAvailableBikeSpot());
    }


    /*Fills the parking spots which has critical section while updating stats cache , hence Lock is used to
     synchronize the cache update
    */
    public void acquireSpot(VehicleType vehicleType, Parking parking, String vehicleNumber, CurrentParkingStat currentParkingStat) {

        ParkingFloor[] numberOfFloors = parking.getParkingFloors();
        Boolean slotFilled = false;
        for (ParkingFloor parkingFloor : numberOfFloors) {
            for (ParkingRow parkingRow : parkingFloor.getParkingRows()) {
                if (VehicleType.BUS.toString().equalsIgnoreCase(vehicleType.toString()) && fillBusSpot(parkingRow,
                        vehicleNumber)) {
                    slotFilled = true;
                    updateAndPublishStatsAfterBusSpotUpdate(currentParkingStat);
                }
                if (VehicleType.BIKE.toString().equalsIgnoreCase(vehicleType.toString()) && fillBikeSpot(parkingRow,
                        vehicleNumber)) {
                    slotFilled = true;
                    updateAndPublishStatsAfterBikeSpotUpdate(currentParkingStat);
                }
                if (VehicleType.CAR.toString().equalsIgnoreCase(vehicleType.toString()) && fillCarSpot(parkingRow,
                        vehicleNumber)) {
                    slotFilled = true;
                    updateAndPublishStatsAfterCarSpotUpdate(currentParkingStat);
                }
                if (slotFilled) {
                    break;
                }
            }
            if (slotFilled) {
                break;
            }
        }
        if (!slotFilled) {
            System.out.println("Vehicle could not be parked ");
        }
    }

    public void expireSpot(VehicleType vehicleType, Parking parking, String vehicleNumber, CurrentParkingStat currentParkingStat) {
        ParkingFloor[] numberOfFloors = parking.getParkingFloors();
        boolean slotCleared = false;
        for (ParkingFloor parkingFloor : numberOfFloors) {
            for (ParkingRow parkingRow : parkingFloor.getParkingRows()) {
                if (VehicleType.BUS.toString().equalsIgnoreCase(vehicleType.toString()) && clearBusSpot(parkingRow,
                        vehicleNumber)) {
                    updateAndPublishStatsAfterBusExpires(currentParkingStat);
                    slotCleared = true;
                }
                if (VehicleType.BIKE.toString().equalsIgnoreCase(vehicleType.toString()) && clearBikeSpot(parkingRow,
                        vehicleNumber)) {
                    slotCleared = true;
                }
                if (VehicleType.CAR.toString().equalsIgnoreCase(vehicleType.toString()) && clearCarSpot(parkingRow,
                        vehicleNumber)) {
                    slotCleared = true;
                }
                if (slotCleared) {
                    break;
                }
            }
            if (slotCleared) {
                break;
            }
        }
        if (!slotCleared) {
            System.out.println("Vehicle with this number does not exist");
        }

    }

    private void updateAndPublishStatsAfterCarExpires(CurrentParkingStat currentParkingStat) {
        try {
            lockOnCarExpire.lock();
            currentParkingStat.setAvailableCarSpot(currentParkingStat.getAvailableCarSpot() + 1);
            publish(currentParkingStat);
        } catch (Exception ex) {
            System.out.println("Exception while updating stats cache " + ex.getMessage());
        } finally {
            lockOnCarExpire.unlock();
        }
    }

    private void updateAndPublishStatsAfterBikeExpires(CurrentParkingStat currentParkingStat) {
        try {
            lockOnBikeExpire.lock();
            currentParkingStat.setAvailableBikeSpot(currentParkingStat.getAvailableBikeSpot() + 1);
            publish(currentParkingStat);
        } catch (Exception ex) {
            System.out.println("Exception while updating  stats cache" + ex.getMessage());
        } finally {
            lockOnBikeExpire.unlock();
        }
    }

    private void updateAndPublishStatsAfterBusExpires(CurrentParkingStat currentParkingStat) {
        try {
            lockOnBusExpire.lock();
            currentParkingStat.setAvailableBusSpot(currentParkingStat.getAvailableBusSpot() + 1);
            publish(currentParkingStat);
        } catch (Exception ex) {
            System.out.println("Exception while updating stats cache" + ex.getMessage());
        } finally {
            lockOnBusExpire.unlock();
        }
    }

    public void setParkingStatsPublisher(ParkingStatsPublisher parkingStatsPublisher) {
        this.parkingStatsPublisher = parkingStatsPublisher;
    }


    private boolean clearBusSpot(ParkingRow parkingRow, String vehicleNumber) {
        String[] busSlots = parkingRow.getBusSlots();
        return clearSpot(busSlots, vehicleNumber);

    }


    private boolean clearBikeSpot(ParkingRow parkingRow, String vehicleNumber) {
        String[] bikeSlots = parkingRow.getBikeSlots();
        boolean bikeCleared = clearSpot(bikeSlots, vehicleNumber);
        if (bikeCleared) {
            updateAndPublishStatsAfterBikeExpires(CurrentParkingStat.getInstance());
        } else if (!bikeCleared) {
            bikeCleared = clearCarSpot(parkingRow, vehicleNumber);
        }
        return bikeCleared;
    }

    private boolean clearCarSpot(ParkingRow parkingRow, String vehicleNumber) {
        String[] carSlots = parkingRow.getCarSlots();
        boolean carCleared = clearSpot(carSlots, vehicleNumber);
        if (carCleared) {
            updateAndPublishStatsAfterCarExpires(CurrentParkingStat.getInstance());
        } else if (!carCleared) {
            carCleared = clearBusSpot(parkingRow, vehicleNumber);
            if (carCleared) {
                updateAndPublishStatsAfterBusExpires(CurrentParkingStat.getInstance());
            }
        }
        return carCleared;
    }


    private boolean fillBusSpot(ParkingRow parkingRow, String vehicleNumber) {
        String[] busSlots = parkingRow.getBusSlots();
        boolean exits = checkIfBusWithThisNumberAlreadyExists(busSlots, vehicleNumber);
        if (exits) {
            System.out.println("Bus with this number already parked");
            return false;
        }
        return fillSpots(busSlots, vehicleNumber);
    }


    private boolean fillBikeSpot(ParkingRow parkingRow, String vehicleNumber) {
        String[] bikeSlots = parkingRow.getBikeSlots();
        boolean exits = checkIfBikeWithThisNumberAlreadyExists(bikeSlots, vehicleNumber);
        if (exits) {
            System.out.println("Bike with this number already parked");
            return false;
        }

        boolean spotFilled = fillSpots(bikeSlots, vehicleNumber);
        if (!spotFilled) {
            System.out.println("No bike Slots avialable ...Finding available space  in car slots");
            String[] carSlots = parkingRow.getCarSlots();
            spotFilled = fillSpots(carSlots, vehicleNumber);
            if (spotFilled) {
                System.out.println("Parked in car slot");
                updateAndPublishStatsAfterCarSpotUpdate(CurrentParkingStat.getInstance());
            }
        }
        if (!spotFilled) {
            System.out.println("No car Slots avialable ...Finding available space  in bus slots");
            String[] busSlots = parkingRow.getBusSlots();
            spotFilled = fillSpots(busSlots, vehicleNumber);
            if (spotFilled) {
                System.out.println("Parked in bus slot");
                updateAndPublishStatsAfterBusSpotUpdate(CurrentParkingStat.getInstance());
            }
        }
        return spotFilled;
    }

    private boolean fillCarSpot(ParkingRow parkingRow, String vehicleNumber) {
        String[] carSlots = parkingRow.getCarSlots();
        boolean exists = checkIfCarWithThisNumberAlreadyExists(carSlots, vehicleNumber);
        if (exists) {
            System.out.println("Car with this number already parked");
            return false;
        }
        boolean filledSpot = fillSpots(carSlots, vehicleNumber);
        if (!filledSpot) {
            System.out.println("No car Slots avialable ...Finding available space  in bus slots");
            String[] busSlots = parkingRow.getBusSlots();
            filledSpot = fillSpots(busSlots, vehicleNumber);
            if (filledSpot) {
                updateAndPublishStatsAfterBusSpotUpdate(CurrentParkingStat.getInstance());

            }
            if (!filledSpot) {
                System.out.println("Slots full for parking Row.Chekcing for available slots  in another row");
            }

        }
        return filledSpot;
    }

    private boolean checkIfBusWithThisNumberAlreadyExists(String[] busSlots, String vehicleNumber) {
        boolean exists = false;
        for (String busNumber : busSlots) {
            if (vehicleNumber.equalsIgnoreCase(busNumber)) {
                exists = true;
                break;
            }
        }
        return exists;
    }

    private boolean checkIfBikeWithThisNumberAlreadyExists(String[] bikeSlots, String vehicleNumber) {
        boolean exists = false;
        for (String bikeNumber : bikeSlots) {
            if (vehicleNumber.equalsIgnoreCase(bikeNumber)) {
                exists = true;
                break;
            }
        }
        return exists;
    }

    private boolean checkIfCarWithThisNumberAlreadyExists(String[] carSlots, String vehicleNumber) {
        boolean exists = false;
        for (String carNumber : carSlots) {
            if (vehicleNumber.equalsIgnoreCase(carNumber)) {
                exists = true;
                break;
            }
        }
        return exists;
    }

    private boolean clearSpot(String[] slots, String vehicleNumber) {
        boolean cleared = false;
        for (int i = 0; i < slots.length; i++) {
            if (vehicleNumber.equalsIgnoreCase(slots[i])) {
                slots[i] = null;
                cleared = true;
                System.out.println("Vehicle exit successfully");
                break;
            }
        }
        return cleared;
    }


    private boolean fillSpots(String[] slots, String vehicleNumber) {
        boolean filled = false;
        for (int i = 0; i < slots.length; i++) {
            if (vehicleNumber.equalsIgnoreCase(slots[i])) {
                System.out.println("Vehicle with this number already parked");
                return false;
            }
            if (slots[i] == null) {
                slots[i] = vehicleNumber;
                filled = true;
                System.out.println("Vehicle parked");
                break;
            }
        }
        return filled;
    }

    //Update parking stats cache and publish parking stats after car has parked
    private void updateAndPublishStatsAfterCarSpotUpdate(CurrentParkingStat currentParkingStat) {

        try {
            lockOnCarAquire.lock();
            if (currentParkingStat.getAvailableCarSpot() > 0) {
                currentParkingStat.setAvailableCarSpot(currentParkingStat.getAvailableCarSpot() - 1);
                publish(currentParkingStat);
            }
        } catch (Exception ex) {
            System.out.println("Exception while updating stats for car ");
        } finally {
            lockOnCarAquire.unlock();
        }

    }

    //Update parking stats cache and publish parking stats after bike has parked
    private void updateAndPublishStatsAfterBikeSpotUpdate(CurrentParkingStat currentParkingStat) {
        try {
            lockOnBikeAquire.lock();
            if (currentParkingStat.getAvailableBikeSpot() > 0) {
                currentParkingStat.setAvailableBikeSpot(currentParkingStat.getAvailableBikeSpot() - 1);
                publish(currentParkingStat);
            }
        } catch (Exception ex) {
            System.out.println("Exception while updating stats for bike");
        } finally {
            lockOnBikeAquire.unlock();
        }
    }

    //Update parking stats cache and publish parking stats after bus has parked
    private void updateAndPublishStatsAfterBusSpotUpdate(CurrentParkingStat currentParkingStat) {
        try {
            lockOnBusAquire.lock();
            if (currentParkingStat.getAvailableBusSpot() > 0) {
                currentParkingStat.setAvailableBusSpot(currentParkingStat.getAvailableBusSpot() - 1);
                publish(currentParkingStat);
            }
        } catch (Exception ex) {
            System.out.println("Exception while updating stats for bus");
        } finally {
            lockOnBusAquire.unlock();
        }
    }
    //Publish the stats to stats topic
    private void publish(CurrentParkingStat currentParkingStat) {
        parkingStatsPublisher.publish(currentParkingStat);
    }
}
