package com.shivendra.parking.service;

import com.shivendra.parking.caching.CurrentParkingStat;
import com.shivendra.parking.domain.Parking;
import com.shivendra.parking.domain.VehicleType;

/**
 * Created by spsingh on 7/11/19.
 */
public interface ParkingService {


    void getParkingStat(CurrentParkingStat currentParkingStat);

    void acquireSpot(VehicleType vehicleType, Parking parking, String vehicleNumber, CurrentParkingStat currentParkingStat);

    void expireSpot(VehicleType vehicleType, Parking parking, String vehicleNumber, CurrentParkingStat currentParkingStat);
}
