package com.shivendra.parking;

import com.shivendra.parking.caching.CurrentParkingStat;
import com.shivendra.parking.domain.Parking;
import com.shivendra.parking.domain.VehicleType;
import com.shivendra.parking.service.ParkingService;
import com.shivendra.parking.service.ParkingServiceImpl;

import java.util.Scanner;

/**
 * The (@code ParkingSystem) class represents the parking system .It is an Entry class that
 * first constructs the parking with given inputs of floor levels and count of each slots of
 * vehicles in all the rows that are there in each floor.
 * Once the parking is contructed it takes vehicle for entry , exit , or knowing the parking status
 * and parks the vehicle in deignated slots.Number of slots for bus is fixed as 5 in each row
 */
public class ParkingSystem {

    private static final String VEHICLE_ENTRY = "e";
    private static final String VEHICLE_EXIT = "x";
    private static final String SLOTS_STATS = "h";
    private static final String SHUT_DOWN = "stop";

    public static void main(String[] args) {

        int numberOfFloors;
        int numberOfRowsPerFloor;
        int numberOfBikeSpotPerRow;
        int numberOfCarSpotPerRow;
        // this is constant as per problem statement
        int numberOfBusSpotPerRow = 5;


        System.out.println("        ***Welcome to Car Parking Design****     ");
        System.out.println();
        System.out.println("Lets First Construct the Parking space !!!");
        Scanner sc = new Scanner(System.in);
        try {
                System.out.println("Enter the number of floors in the parking system");
                numberOfFloors = sc.nextInt();
                System.out.println("Enter the number of Rows per floor");
                numberOfRowsPerFloor = sc.nextInt();
                System.out.println("Enter the number of bike spots per row");
                numberOfBikeSpotPerRow = sc.nextInt();
                System.out.println("Enter the number of Car spots per Row");
                numberOfCarSpotPerRow = sc.nextInt();
                ParkingService parkingService = new ParkingServiceImpl();



                //initliaze and get the singleton instance of parking and stats class

                Parking.init(numberOfFloors);
                Parking parking = Parking.getInstance();
                parking.construct(numberOfFloors, numberOfRowsPerFloor, numberOfBikeSpotPerRow, numberOfCarSpotPerRow, numberOfBusSpotPerRow);
                CurrentParkingStat.init(numberOfRowsPerFloor * numberOfCarSpotPerRow * numberOfFloors,
                        numberOfBikeSpotPerRow * numberOfFloors * numberOfRowsPerFloor, 5 * numberOfRowsPerFloor * numberOfFloors);
                CurrentParkingStat currentParkingStat = CurrentParkingStat.getInstance();

                boolean shutParking=true;
                boolean correctInput =false;
                while (shutParking) {

                    System.out.println("Enter e for entry of vehicle , x for exit and h for stats of parking ,stop to" +
                            " shut parking down");
                    String inputType = sc.next();

                    if (VEHICLE_ENTRY.equals(inputType)) {
                        // scanner for vehicle type
                        correctInput =true;
                        System.out.println("Enter the vehicle type to park amongst: CAR , BUS , BIKE");
                        String vehicleType = sc.next();
                        if (VehicleType.BUS.toString().equalsIgnoreCase(vehicleType)) {
                            System.out.println("Enter the vehicle number");
                            String vehicleNumber = sc.next();
                            parkingService.acquireSpot(VehicleType.BUS, parking, vehicleNumber, currentParkingStat);
                        } else if (VehicleType.BIKE.toString().equalsIgnoreCase(vehicleType)) {
                            System.out.println("Enter the vehicle number");
                            String vehicleNumber = sc.next();
                            parkingService.acquireSpot(VehicleType.BIKE, parking, vehicleNumber, currentParkingStat);
                        } else if (VehicleType.CAR.toString().equalsIgnoreCase(vehicleType)) {
                            System.out.println("Enter the vehicle number");
                            String vehicleNumber = sc.next();
                            parkingService.acquireSpot(VehicleType.CAR, parking, vehicleNumber, currentParkingStat);
                        } else {
                            System.out.println("Only CAR , BUS , BIKE allowed");
                        }
                    }

                    if (VEHICLE_EXIT.equalsIgnoreCase(inputType)) {
                        correctInput =true;
                        System.out.println("Enter the vehicle type to park amongst: CAR , BUS , BIKE");
                        String vehicleType = sc.next();
                        if (VehicleType.BUS.toString().equalsIgnoreCase(vehicleType)) {
                            System.out.println("Enter the vehicle number");
                            String vehicleNumber = sc.next();
                            parkingService.expireSpot(VehicleType.BUS, parking, vehicleNumber, currentParkingStat);
                        } else if (VehicleType.BIKE.toString().equalsIgnoreCase(vehicleType)) {
                            System.out.println("Enter the vehicle number");
                            String vehicleNumber = sc.next();
                            parkingService.expireSpot(VehicleType.BIKE, parking, vehicleNumber, currentParkingStat);
                        } else if (VehicleType.CAR.toString().equalsIgnoreCase(vehicleType)) {
                            System.out.println("Enter the vehicle number");
                            String vehicleNumber = sc.next();
                            parkingService.expireSpot(VehicleType.CAR, parking, vehicleNumber, currentParkingStat);
                        } else {
                            System.out.println("Only CAR , BUS , BIKE allowed");
                        }
                    }

                    if (SLOTS_STATS.equalsIgnoreCase(inputType)) {
                        correctInput =true;
                        parkingService.getParkingStat(currentParkingStat);
                    }

                    if(SHUT_DOWN.equalsIgnoreCase(inputType)){
                        correctInput =true;
                        shutParking=false;
                    }
                    if (!correctInput){
                        System.out.println("Input Incorrect");
                    }
                }

        } catch (Exception ex) {
            System.out.println("Exception while taking input" + ex.getMessage());

        }

    }


}
