# PROJECT
This is a parking Design system that allows Bike,Car and Bus to be parked . It is a multiLevel parking system and number of levels , rows in 
each level and corresponding number of spots for each vehicle is made configurable i.e to be input from console when application is run.This is 
made configurable so that this design is not restricted to  particular dimensions of parking lot.As per problem statement number of bus slots is
kept as 5

#Kafka 
Latest parking statistics i.e. number of slots available for each vehicle is published on topic "stats" so that other microservices can consume
for their business logics e.g. pricing service can consume stats and apply surge prices 

Important Note: If below installation is not completed producer will poll for topic for 60000ms and then will timeout.Complete the below installation
to avoid timeout.Since publishing is for other service ,to test the functionality of parking system and for uninterrupted flow of parking system Work 
around is to comment the line number 361 in ParkingServiceImpl.java if below installation is not complete.

#Installation
 >Download the 2.0.0 release and un-tar it (download link: http://apachemirror.wuchna.com/kafka/2.2.0/kafka_2.12-2.2.0.tgz)
 >Navigate to the folder kafka_2.12-2.2.0 using command line and start the zookeeper by  command 
 bin/zookeeper-server-start.sh config/zookeeper.properties
 >Now on a new terminal tab on same folder  path start the kafka server by command 
 bin/kafka-server-start.sh config/server.properties
 >Now on a new terminal tab on navigate to directory kafka_2.12-2.2.0/bin create a topic 
 :sh kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic stats
 >Now on a new Terminal navigate to kafka_2.12-2.2.0/bin/ and run
 sh kafka-console-consumer.sh --bootstrap-server localhost:9092  --topic stats  --from-beginning
 
#Application entry point and start
 Class ParkingSystem.java is entry point of the application which has main method so that the dimensions of parking lot and vehicle entry/exit
 is entered on console.
 Hence run the ParkingSystem.java to start the application and enter the inputs asked in console.
 
 Cache of slots  has been maintained for each vehicle  in array of String that stores vehicle number parked in each slot and its update 
 for stats i.e crtical section has been synchronized by locks.

#DemoSubscriber
 This service is for consuming stats published by parking lot.
 
#Installation
 This is a springboot web application and hence tomcat server is installed with dependencies.
 
#Application entry point and start
  DemoSubscriberApplication is the application entry point .
  >Run DemoSubscriberApplication.java
  >Go to browser and make a get request http://localhost:8080/subcribe(Service is running on port 8080).This will keep consumer polling 
  the topic for message.
  
  Now run and make entry in parking system and publish stats . published stats can be seen in consumer console .These stats can
  be used for various business logics.
 
 
#Test Cases
 To Ensure correct output for various inputs test cases have been covered in Junit and mockito in parking system.
 
 
 
 
 
 
 
 
 