package com.example.demo;

/**
 * Singleton CurrentParkingStat for keeping record of parking slots as cache
 * so that this cache can be published to be consumed by other microservices for
 * business logics like price surging or display etc
 */
public class CurrentParkingStat {
    private static CurrentParkingStat currentParkingStat;
    private int availableCarSpot;
    private int availableBikeSpot;
    private int availableBusSpot;

    //Singleton Instance
    private CurrentParkingStat(int availableCarSpot, int availableBikeSpot, int availableBusSpot) {
        this.availableCarSpot = availableCarSpot;
        this.availableBikeSpot = availableBikeSpot;
        this.availableBusSpot = availableBusSpot;
    }

    public static CurrentParkingStat getInstance() {
        if (currentParkingStat == null) {
            System.out.println("initialize first ");
        }

        return currentParkingStat;
    }

    public synchronized static CurrentParkingStat init(int availableCarSpot, int availableBikeSpot, int availableBusSpot) {
        if (currentParkingStat == null) {
            currentParkingStat = new CurrentParkingStat(availableCarSpot, availableBikeSpot, availableBusSpot);
        }

        return currentParkingStat;
    }

    public int getAvailableCarSpot() {
        return availableCarSpot;
    }

    public void setAvailableCarSpot(int availableCarSpot) {
        this.availableCarSpot = availableCarSpot;
    }

    public int getAvailableBikeSpot() {
        return availableBikeSpot;
    }

    public void setAvailableBikeSpot(int availableBikeSpot) {
        this.availableBikeSpot = availableBikeSpot;
    }

    public int getAvailableBusSpot() {
        return availableBusSpot;
    }

    public void setAvailableBusSpot(int availableBusSpot) {
        this.availableBusSpot = availableBusSpot;
    }

}
