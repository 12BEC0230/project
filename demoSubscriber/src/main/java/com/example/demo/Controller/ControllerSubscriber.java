package com.example.demo.Controller;

import com.example.demo.Service.SubscriberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * Created by spsingh on 8/7/18.
 */
@Controller
public class ControllerSubscriber {

    @Autowired
    SubscriberService subscriberService;


    @GetMapping(value="/subcribe")
    public void getMessage()
    {
subscriberService.subscribe();
    }
}
