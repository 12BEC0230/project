package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

/**
 * Created by spsingh on 7/12/19.
 */
public class ParkingStatsDeserializer implements Deserializer<CurrentParkingStat> {
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    public CurrentParkingStat deserialize(String s, byte[] data) {
        ObjectMapper mapper = new ObjectMapper();
        CurrentParkingStat parkingStat = null;
        try {
            parkingStat = mapper.readValue(data, CurrentParkingStat.class);
        } catch (Exception exception) {
            System.out.println("Error in deserializing bytes " + exception);
        }
        return parkingStat;
    }

    public CurrentParkingStat deserialize(String topic, Headers headers, byte[] data) {
        return null;
    }

    public void close() {

    }
}
