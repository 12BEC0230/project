package com.example.demo.Service;

import org.apache.kafka.common.serialization.LongDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Properties;

/**
 * Created by spsingh on 8/7/18.
 */
@Service
public class SubscriberConfigurations {

    @Value("${bootstrap.servers}")
            String bootstrapServers;
    Properties properties;

    Properties configurations()
    {
        properties=new Properties();
        properties.put("bootstrap.servers",bootstrapServers);
        properties.put("key.deserializer", LongDeserializer.class.getName());
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("group.id","group");
        properties.put("auto.offset.reset","earliest");

        return properties;
    }
}
