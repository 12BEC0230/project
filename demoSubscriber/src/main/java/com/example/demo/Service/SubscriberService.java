package com.example.demo.Service;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Properties;

/**
 * Created by spsingh on 8/7/18.
 */
@Service
public class SubscriberService {

    @Autowired
    SubscriberConfigurations subscriberConfigurations;

    @Value("${topic}")
    String topic;

    Properties properties;

    public void subscribe()
    {
        properties=subscriberConfigurations.configurations();
        KafkaConsumer<Long,String> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(Collections.singletonList(topic));
        while (true) {
            ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);
            consumerRecords.forEach(record -> {
                System.out.printf("Consumer Record:(%d, %s, %d, %d)\n",
                        record.key(), record.value(),
                        record.partition(), record.offset());
            });

        }

    }

}
